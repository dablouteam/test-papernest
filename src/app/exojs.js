function renderMoney(price, coinAndBankNotes){
    let i = coinAndBankNotes.length - 1;
    const countingObj = coinAndBankNotes.reduce((obj, coin) => {
      obj[coin] = 0;
      return obj;
    }, {});
  
    while (i >= 0) {      
      if (price - coinAndBankNotes[i] >= 0) {
        price -= coinAndBankNotes[i];
        countingObj[coinAndBankNotes[i]]++;
      } else {
        i--;
      }
    }    

    let total = countingObj['2'] + countingObj['5'] + countingObj['10'];

    if(total == 0 || price  > 0) {
        return null;
    
    }else{
        return countingObj;
    }
}

console.log(renderMoney( 37, [2, 5 , 10]));
