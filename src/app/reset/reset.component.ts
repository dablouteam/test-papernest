import { Component, OnInit } from '@angular/core';
import { CounterService } from '../counter.service';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent implements OnInit {

  inputDate:string = '';

  constructor(private counterS : CounterService) { }

  ngOnInit(): void {

  }

  clickButton(){

    var today = new Date();
     
    var years = this.inputDate.substr(0,4);                                        // recuperation de l'année (les quatre premiers caractères)
    var month = this.inputDate.substr(5,2);                                      // recuperation du mois 
    var day = this.inputDate.substr(8,2);                                       // recuperation du jour
    var dateNaissance = new Date(years + "-" + month + "-" + day);

    var age = today.getFullYear() - dateNaissance.getFullYear();                // calcul  des années passées
    
    var mois = today.getMonth() - dateNaissance.getMonth();                     // calcul  des mois passées

    
    if (mois< 0 || (mois === 0 && today.getDate() < dateNaissance.getDate())) {
        age = age - 1;
    }

    if(age >= 18 ){
      this.counterS.resetCounter();
      localStorage.clear();
    }    
     
    
  }

}
