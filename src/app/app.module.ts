import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { UpComponent } from './up/up.component';
import { DownComponent } from './down/down.component';
import { RouterModule, Routes } from '@angular/router';
import { ResetComponent } from './reset/reset.component';
import { FormsModule } from '@angular/forms';


export const AppRoutes: Routes = [
	  { path: '', component: UpComponent },
    { path: 'up', component: UpComponent },
    { path: 'down', component: DownComponent},	
    { path: 'reset', component: ResetComponent},	
];

@NgModule({
  declarations: [
    AppComponent,
    UpComponent,
    DownComponent,
    ResetComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(AppRoutes, { relativeLinkResolution: 'legacy' })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
