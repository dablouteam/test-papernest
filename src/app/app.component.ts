import { Component, OnInit } from '@angular/core';
import { CounterService } from './counter.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'test-papernest';

  counter : number = 1;

  backgroundColor : string = 'white';

  constructor(private counterService: CounterService) { }

  ngOnInit(): void {

    // changement de background colorquand le compteur est à 10 ou -10
      this.counterService.counter$.subscribe((counter) => {      
        this.counter = counter;

        if(counter === 10 ){
          this.backgroundColor = "#e74c3c";
        } else if(counter === -10){
          this.backgroundColor = "#27ae60";
        }else{
          this.backgroundColor = 'white';
        }

       });

  }

}
