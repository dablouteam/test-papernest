import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CounterService {
  
  additionalValue: number = 1;                                          // Valeur absolue à ajouter
  counter$ : BehaviorSubject<number> = new BehaviorSubject(1);          // Stocke et partage les données du compteur
  counterActions: number = 1;                                           // Compte le nombre de click (up ou down)
 
  constructor() {

    if(localStorage.getItem('counter') !){                              // récuperation de la pécédente valeur sauvegardé dans ls cookies, s'il existe
      const initialeCompteur = parseInt(localStorage.getItem('counter')!);
      this.counter$.next(initialeCompteur)                              // initialisation du compteur
    }
   }

  setNewValue(value: number){
    this.counterActions++;

    if(this.isMultiple(this.counterActions , 30)){                      // vérifie si le compteur total des actions est un multiple de 30
      this.additionalValue = 2 * value;                                 // je pense que c'est bon
    }
    
    if(this.counter$.value === 0){                                      // Si compteur est null, 
       this.counter$.next(value);                                       // on assigne directement la valeur reçu par le composant
    }else{
      if(  value > 0 ){
        this.counter$.next(this.counter$.value + this.additionalValue);
      }else{
        this.counter$.next(this.counter$.value - this.additionalValue);
      }
    }

   localStorage.setItem('counter', this.counter$.value.toString())      // stockage en local de la valeur du compteur
  }

  /**
   * Vérifie si c'est un multiple de 30
   * @param nbAction 
   * @param multipleToCompare 
   * @returns boolean
   */
  isMultiple(nbAction: number, multipleToCompare: number): boolean {    
    return Math.round(nbAction / multipleToCompare) / (1 / multipleToCompare) === nbAction; 
  }

  /**
   * reset du compteur par le composant Reset
   */
  resetCounter(): void {                                                       
    this.counter$.next(0)
  }
  
}
