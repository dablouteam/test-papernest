import { Component, OnInit } from '@angular/core';
import { CounterService } from '../counter.service';

@Component({
  selector: 'app-down',
  templateUrl: './down.component.html',
  styleUrls: ['./down.component.css']
})
export class DownComponent implements OnInit {
  

  additionalValue: number = 1;

  constructor(private counterService: CounterService) { }

  ngOnInit(): void { 
      this.additionalValue= this.counterService.additionalValue;
  }

  decrement(){
    this.counterService.setNewValue(-this.additionalValue);
  }

}
