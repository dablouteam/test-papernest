import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CounterService } from '../counter.service';

@Component({
  selector: 'app-up',
  templateUrl: './up.component.html',
  styleUrls: ['./up.component.css']
})
export class UpComponent implements OnInit {

  additionalValue: number = 1;
  
  constructor(private counterService: CounterService) { }

  ngOnInit(): void { 
    this.additionalValue= this.counterService.additionalValue;
  }

  increment(){
    this.counterService.setNewValue(this.additionalValue);
  }
}
